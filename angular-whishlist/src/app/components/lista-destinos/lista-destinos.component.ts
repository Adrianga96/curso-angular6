import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinosAPiClient } from '../../models/destinos-api-client-model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store'
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosAPiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosAPiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
    this.store.select(state => state.destinos.eliminar)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se ha eliminado a ' + d);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items)
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(d: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}

//Implementar reinicio de votos y eliminación de objetos